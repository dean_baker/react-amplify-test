This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Below you will find some information on how to perform common tasks.<br>
You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

This project is designed to give the most basic logged in experience and API connectivity
using [AWS Amplify](https://aws-amplify.github.io/). 


## Starting state

The application has been built quickly, and currently has no tests. There are the following features:
* Sign up - with confirmation code sent via email
* Login
* Home page
* Profile page (authenticated page)
* Welcome page (authenticated page)


There is no code currently to connect to an API. 

## The challenge

We would like to see the _Profile_ page connect to an API to send some data to an API using the AWS Amplify library.
We have made an API with the following attributes:
```
Name:  profileCRUD
Path:  profile
```
We have set this codebase up with everything you need to connecto to the API - you do not need to configure
anything - you just need to create the code.
[This guide](https://aws-amplify.github.io/amplify-js/media/api_guide) should help you out, and as always
you can contact me by email or chat to iron out any issues you are having with the environment.

### The requirements
Enhance the /profile page to allow a user to enter the following details:

|Field|Type|Comments|
|---|---|---|
|Date of Birth | Date field | This will be translated into the standard ISO date format for storage |
|Income | Number field | This will be translated into a positive integer for storage |
|Income Frequency | Select One | User will select between weekly, fortnightly, monthly |
| Permanent Resident | Boolean | User will select yes or no |

All fields are mandatory

On page load if there is data available for the user, the form should be pre populated.

####The Payload
The api will expect a payload to look something like this:
```json
{
  "dob": "INSERT VALUE HERE",
  "income": "INSERT VALUE HERE",
  "incomeFrequency": "INSERT VALUE HERE",
  "permanentResident": "INSERT VALUE HERE"
}

```

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](#running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!
