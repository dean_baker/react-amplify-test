import React, {Component} from 'react';
import './App.css';
import {BrowserRouter as Router, Route} from "react-router-dom";
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import Amplify, {Auth} from 'aws-amplify';
import config from './aws-exports';

import Navbar from './layout/Navbar';
import Footer from './layout/Footer';
import Routes from './routes';

import * as actions from './actions/authAction'

Amplify.configure(config);


class App extends Component {


    componentWillMount() {
        Auth.currentAuthenticatedUser()
            .then((user) => {
                this.props.actions.login(user);
            })
            .catch(() => {
            })
    }

    logout = () => {
        console.log('logging out');
        console.log(this.props);
        Auth.signOut()
            .then(() => {
                // this.props.actions.logout();
                this.props.history.push("/");
            })
            .catch((err) => {
                console.log('Error logging out');
                console.log({err});

            });
    };

    render() {
        let childProps = {
            isAuthenticated: this.props.auth.user != null
        };
        return (

            <Router>
                <div className="App">
                    <Route path="*" component={Navbar}/>
                    <Routes childProps={childProps}/>
                    <Route path="*" component={Footer}/>
                </div>
            </Router>
        );
    }
}

const mapStateToProps = state => ({
    ...state
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
});


export default connect(mapStateToProps, mapDispatchToProps)(App);

