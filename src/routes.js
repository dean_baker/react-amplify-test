import React from 'react';
import {Switch} from "react-router-dom";
import AppliedRoute from './components/routes/AppliedRoute';
import UnauthenticatedRoute from './components/routes/UnauthenticatedRoute';
import AuthenticatedRoute from './components/routes/AuthenticatedRoute';

import Landing from "./pages/Landing";
import {Login, Signup} from "./pages/Auth";
import Profile from "./pages/Profile";
import Welcome from './pages/Welcome';
const Routes = ({childProps}) => {
    return (
        <Switch>
            <AppliedRoute exact path="/" component={Landing} props={childProps}/>
            <UnauthenticatedRoute path="/login" component={Login} props={childProps}/>
            <UnauthenticatedRoute path="/signup" component={Signup} props={childProps}/>
            <AuthenticatedRoute path="/profile" component={Profile} props={childProps}/>
            <AuthenticatedRoute path="/welcome" component={Welcome} props={childProps}/>
        </Switch>
    );
};

export default Routes;