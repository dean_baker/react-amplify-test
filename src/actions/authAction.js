
export const login = (userDetails) => {
    return (dispatch) => {
        return dispatch({
            type: 'LOGGED_IN',
            user: userDetails
        })
    }
};

export const logout = () => {
    return (dispatch) => {

        return dispatch({
            type: 'LOGOUT'
        });
    }
};


