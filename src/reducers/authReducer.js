export default (state = {}, action) => {
    switch (action.type) {
        case 'LOGGED_IN':
            return {
                user: action.user
            }
        case 'LOGOUT':
            return {
                user: undefined
            };
        default:
            return state
    }
}
