
import {Navbar, Nav, NavItem} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {LinkContainer} from 'react-router-bootstrap';

import logo from './logo.svg';
import './style.css';

import {Auth} from 'aws-amplify';




import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import * as actions from "../../actions/authAction";


class NavbarBanner extends Component {


    logout = () => {
        Auth.signOut()
            .then(() => {
                this.props.actions.logout();
                this.props.history.push("/");
            })
            .catch((err) => {
                console.log('Error logging out');
                console.log({err});

            });
    };

    authActionButtons = () => {

        const auth = this.props.auth;
        if (auth && auth.user) {
            return (
                <Nav pullRight>
                    <LinkContainer to="/profile">
                        <NavItem eventKey={1} href="#">
                            My Profile
                        </NavItem>
                    </LinkContainer>
                    <NavItem onClick={this.logout}>Logout</NavItem>
                </Nav>
            )
        }
        else {
            return (
                <Nav pullRight>
                    <LinkContainer to="/login">
                        <NavItem href="#">
                            Login
                        </NavItem>
                    </LinkContainer>
                    <LinkContainer to="/signup">
                        <NavItem href="#">
                            Sign Up
                        </NavItem>
                    </LinkContainer>
                </Nav>
            )

        }
    };

    render() {

        console.log('navbar page');
        console.log(this.props);
        return (
            <Navbar collapseOnSelect>
                <Navbar.Header>
                    <Link to="/" href="#">
                        <img id="logo" src={logo} alt="logo"/>
                    </Link>
                    <Navbar.Toggle/>
                </Navbar.Header>
                <Navbar.Collapse>
                    {this.authActionButtons()}
                </Navbar.Collapse>
            </Navbar>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
});

const mapStateToProps = state => ({
    ...state
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NavbarBanner);



